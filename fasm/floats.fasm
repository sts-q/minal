;============================================================================

; :file:   floats.asm                   Minal floating point

;============================================================================

FPU_IE =   0                                    ; invalid operand
FPU_ZE =   2                                    ; zero error: zero divide
FPU_SF =   6                                    ; stack fault: overflow or underflow
FPU_ES =   7                                    ; error summary
FPU_C0 =   8                                    ; C0
FPU_TOP = 11                                    ; top-of-stack pointer

FPU_CCC_EMPTY = (64 + 1)                        ; condition code: empty


; get current load of FPU stack
; TOP is zero when stack is full and when stack is empty
; assumption: sp(0) is empty --> all regs are empty
;             (TOP == 0) and (sp(0) is empty)  -->  stack is empty
;             (TOP == 0) and (sp(0) is full )  -->  stack is full
; http://www.website.masmforum.com/tutorials/fptute/fpuchap1.htm
; https://www.felixcloutier.com/x86/fxam
macro get_fs_current {
    local .ok
        clr a                                   ; b := 8 - stackpointer
        fnstsw ax
        and ax, (7 shl FPU_TOP )
        shr ax, FPU_TOP
        mov b, 8
        sub b, a
        cmp a, 0                                ; TOP != zero  -->  done
        jne .ok
        fxam                                    ; examin sp(0): is it empty?
        fnstsw ax
        and ax, (( 64 + 4 + 1 ) shl FPU_C0)     ; get C3 C2 C0
        shr ax, FPU_C0
        cmp ax, FPU_CCC_EMPTY                   ; compare with condition code empty
        jne .ok
        clr b
     .ok:
        mov a,b
}


macro check_fs_underflow {
        fnstsw ax
        and ax, (1 shl FPU_SF)
        jnz Error.fs_underflow
}

macro check_fs_overflow {
        fnstsw ax
        and ax, (1 shl FPU_SF)
        jnz Error.fs_overflow
}

macro check_fs_ie {                             ; invalid operand
        fnstsw ax
        and ax, (1 shl FPU_IE)
        jnz Error.fs_ie
}


macro fdup {
        fld st0
}
macro fswap {
        fxch
}
macro fover {
        fld st1
}


macro load_flt r {
        unflt r
        mov [flt_1], qword r
        fld qword [flt_1]
}

macro load_int r {
        unint r
        mov [flt_1], qword r
        fild qword [flt_1]
}

macro fload_x r, name {
   local .noint, .err
        noint? r, .noint
        load_int r
        jmp @f
.noint: noflt? r, .err
        load_flt r
        jmp @f
    .err: caa errmess_nointorflt, name, r
    @@:
}

macro unload_flt r {
        fstp qword [flt_1]
        mov r, qword [flt_1]
        check_fs_ie
        flt r
}

macro unload_int r {
        fistp qword [flt_1]
        mov r, qword [flt_1]
        check_fs_ie
        int r
}


;============================================================================
macro Floats.init { calls Floats.initF }
Floats.initF:
                fninit
                fnclex
                mov rax, 1
                mov [aone], rax
                ret


;;;Error.fs_underflow:             error errmess_fs_underflow
;;;Error.fs_overflow:              error errmess_fs_overflow
Error.fs_ie:                    error errmess_fs_ie


;============================================================================

