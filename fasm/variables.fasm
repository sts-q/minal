;============================================================================

; :file:   variables.asm               Minal Data and BSS

;============================================================================
section '.data' writeable  align 64                        ; initialized data

;-----------------------------------------------------------------------------
__data_first__:

; GTD -- Global descriptor table entry format
; See Intel 64 Software Developers' Manual, Vol. 3A, Figure 3-8
; or http://en.wikipedia.org/wiki/Global_Descriptor_Table
; 21 L          1 -> long mode   D/B must be 0
; 22 D/L        0       default op size

macro GDT_ENTRY base, limit, flags, type {
               ; %1 is base address, %2 is segment limit, %3 is flags, %4 is type.
               dw   limit and 0xffff                                    ;       16   limit 15:0
               dw   base  and 0xffff                                    ;       16   base 15:0
               db   (base shr 16) and 0xff                              ; 0..7   8   base 23:16
               db   type or ((flags shl 4) and 0xf0)                    ; 8..15  8   tttt s dd p
               db   (flags and 0xf0) or ((limit shr 16) and 0x0f)       ; 16..23 8   GDLA limit-19:16
               db   base shr 24                                         ; 24..31 8   base 31:24
}

EXECUTE_READ   =     1010b
READ_WRITE     =     0010b
RING0          = 10101001b             ; Flags set: Granularity, 64-bit, Present, S; Ring=00

; Global descriptor table (loaded by lgdt instruction)
;;;gdt_hdr:
;;;            dw   gdt_end - gdt - 1
;;;            dd   gdt
;;;gdt:
;;;            GDT_ENTRY 0, 0, 0, 0
;;;            GDT_ENTRY 0, 0xffffff, RING0, EXECUTE_READ
;;;            GDT_ENTRY 0, 0xffffff, RING0, READ_WRITE
;;;gdt_end:


;-----------------------------------------------------------------------------
; :section:             GDT
;;;Intel® 64 and IA-32 Architectures
;;;Software Developer’s Manual
;;;Volume 3A:
;;;System Programming Guide, Part 1
;;; 5.2.1       Code-Segment Descriptor in 64-bit Mode

; G                     Granularity: 1 --> 4kB ... 4GB   0 --> 1 Byte ... 1 MByte
; CS.D := 0             Default  (because CS.L == 1)  --> default address-size = 32, default operand-size = 64
; CS.L := 1             IA-32e 64-bit
; a AVL                 available to system programmer
; P                     present
; d DPL                 descriptor privilege level
; C                     conforming: 1 --> call only segments of the same privilege level
; R                     readable
; A                     accessed

macro gdt_empty_entry    {
                dd 0
                dd 0
}

macro gdt_code_entry    {
                dd 0
                db 0
                dw 1010000010011010b
;;;             ;  GDLa0000Pd.11CRA
                db 0

}

macro gdt_data_entry {
}

macro gdt_tss_entry {
                dd 0                    ; zero or reserved
                dd 0                    ; base address 63:32

                db 0                    ; base address  31:24
                db 10001111b            ; G00allll              limit 19:16
                db 10001010b            ; Pd.0tttt
                db 0                    ; base address  23:16

                dw 0                    ; base address  15:0
                dw 0xffff               ; segment limit 15:0
}

gdt_hdr:
                dw gdt_end - gdt - 1
                dd gdt

align 64
gdt:
                gdt_empty_entry
                gdt_code_entry
                GDT_ENTRY 0, 0xffffff, RING0, READ_WRITE
                gdt_tss_entry
gdt_end:


;-----------------------------------------------------------------------------
; :section:             var IDT

idt_desc:
    .idt_size:                  dw (IDT_end - IDT )     ; 2 bytes size
    .idt_location:              dq IDT                  ; 8 bytes location

align 64
IDT:
                                rb (64 * 16)
IDT_end:

isr_work_running                rd 1
vm_is_ready:                    rd 1


;-----------------------------------------------------------------------------

;;;align 4
;;;kernel_stack_bottom:           dd stack_
;;;kernel_stack_size:             dd (RSTACK_SIZE * ws)

__data_last__:

;============================================================================
section '.bss' writeable align 64                       ; not initialized data ( no space in ELF used )

__var_first__:

;-----------------------------------------------------------------------------
; :section:             var stacks

align 8
RSTACK_SIZE                    = 1 * KiB * KiB
stack_:                        rq RSTACK_SIZE
stack_end:

DSTACK_SIZE                    = 256
border_1:                       rq 1
dstack:                         rq DSTACK_SIZE
border_2:                       rq 1
sstack:                         rq DSTACK_SIZE
border_3:                       rq 1

;-----------------------------------------------------------------------------
; :section:             var list

align 64
GCWALL                          =  9999222233331111
;;;maximal_list_elements        =  48 * KiB
maximal_list_elements           =   4 * KiB * KiB
list_mem:                       rb 12 * maximal_list_elements
list_mem_end:


;-----------------------------------------------------------------------------
; :section:             var array

stringbuffer_size       = 64 * KiB
stringbuffer:           rb stringbuffer_size
stringbuffer_end:

;-----------------------------------------------------------------------------
; :section:             var symtab

align 8
symtab_maximal          =  10000
tFunc_size              =  7 * ws
o_fn                    =  0                            ; sym:sym
o_name                  =  8                            ; lst
o_docstring             =  16                           ; lst
o_pSrc                  =  24                           ; lst
o_line                  =  32                           ; int:int
o_size                  =  40                           ; int:i  ! inc qword ...
o_count                 =  48                           ; int:i  ! ind qword ...
symtab_last             =  symtab + (symtab_maximal - 1) * tFunc_size
symtab:                 rb tFunc_size * symtab_maximal
symtab_ff               rd 1                            ; first free element

sym_nodef:              rq 1                            ; atom address of nodef: Symtab.init
sym_docstring:          rq 1
sym_openparen:          rq 1
sym_closeparen:         rq 1
sym_true:               rq 1
sym_false:              rq 1


;-----------------------------------------------------------------------------
; :section:             var parse

Parse.current_error:    rq 1            ; str
Parse.current_source:   rq 1            ; lst
Parse.current_line:     rq 1            ; int
Parse.strange_chr:      rq 1            ; chr

; register usage of parser:
;
;       k       kurrent position in source-string, source string is zero-terminated
;       v       start position of thing in source string: word: (cons (word v (k-1)) ts)
;       c       current char: c = byte [k]
;       d       string start char
;       sos     thing to cons
;       tos     ts: list of tokens we would like to get from source string
;       a,b     not_in, skip, skip_until
;       si      bool: sign within cons_word_or_number
;       di      bool: sign chr within cons_word_or_number


;-----------------------------------------------------------------------------
; :section:             gc

GC_BIT                  =  31

GC_NOW_FLAG:            rq 1
GC_NOW_PINGCHR_BUFFER:  rq 1
List.gc.runs:           rq 1


;-----------------------------------------------------------------------------
; :section:             var VGA

vga_mem                        = 0b8000h
vga_line:                      rq 1
vga_col:                       rq 1                     ; pointing free
vga_attr:                      rq 1                     ; vga_attr := attr << 8  -->  bx := chr+attr; vga[]:=bx
vga_indentation:               rq 1                     ; at carriage return, the carriage stops at indentation


;-----------------------------------------------------------------------------
; :section:             var others
__var_midd__:

multiboot_info:                rd 1                     ; we will use this in kernel's main
magic:                         rd 1                     ; we will use this in kernel's main

kbd_buffer:                     rb 128                  ; ringbuffer for keyboard scancodes (2 bytes per key: A --> 4 bytes)
kbd_buffer_end:

iprint_buffer:                  rb 48                   ; temp buffer for digits to be printed
iprint_buffer_end:


kbd_read_ptr:                   rq 1
kbd_write_ptr:                  rq 1

;-----------------------------------------------------------------------------
status_line_state:              rd 1
status_line_topic:              rd 1
status_line_error:              rd 1

flt_1:                          rq 2
aone:                           rq 1

align 64
count_exec_atom:                rq 1
count_exec_def:                 rq 1
count_exec_pushlit:             rq 1
count_cons:                     rq 1

rtc_count:                      rq 1
rtc_sleep_until:                rq 1
;;;rsp_check:                      rq 1
pitticks:                       rq 1


__var_last__:
;============================================================================
; 2021-04-06

