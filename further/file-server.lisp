;;;===========================================================================

;;; :file:   file-server.lisp           File server for Minal-OS

;;;===========================================================================

(format t "Hello, this is the Minal file-server.~%")
(format t "Exit with Ctrl-c.~%")
(format t "~%")


;;;===========================================================================
(defparameter   f       "inc/scratch.minal")
(defvar         line    "")
(defvar         fd      nil)


(defun open-file( name )
        (setf fd (open name     :direction :output
                                :if-does-not-exist :create
                                :if-exists :supersede)))

(defun close-file()
        (when fd (close fd))
        (setf fd nil))

(defun info()
        (cond
          (fd   "C")
          (t    "-")))

(defun cmd(  line )  (search ":COMPORT COMMAND FILE REWRITE:"   line))
(defun done( line )  (search ":COMPORT COMMAND DONE:"           line))


;;;===========================================================================
(loop
        (setf line (read-line))
        (cond
                ((cmd line)     (open-file f))
                ((done line)    (close-file))
                (fd             (format fd "~A~%" line) (finish-output fd))
                (t              )
                )
        (format t "[~A] ~A~%" (info) line)
        )


;;;===========================================================================
;;; (c) sts-q 2021-Jun
